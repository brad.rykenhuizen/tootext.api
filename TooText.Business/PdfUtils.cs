﻿using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Kernel.Pdf.Canvas.Parser.Listener;
using System.IO;
using System.Text;

namespace TooText.Business
{
    public class PdfUtils
    {
        public static string ExtractTextFromPdf(Stream pdfStream)
        {

            using var pdfDocument = new PdfDocument(new PdfReader(pdfStream));
            var text = new StringBuilder();
            
            for (int i = 1; i <= pdfDocument.GetNumberOfPages(); i++)
            {
                var page = pdfDocument.GetPage(i);
                string pageText = PdfTextExtractor.GetTextFromPage(page, new LocationTextExtractionStrategy());

                text.Append(string.Concat(pageText.Split('\n')));
            }

            return text.ToString();
        }
    }
}
