﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TooText.Business;


namespace TooText.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PdfController : ControllerBase
    {

        private readonly ILogger<PdfController> _logger;

        public PdfController(ILogger<PdfController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public string Post()
        {
            return PdfUtils.ExtractTextFromPdf(Request.Body);
        } 
    }
}
